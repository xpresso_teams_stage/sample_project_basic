"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Naveen Sinha"

import time
import sys

if __name__ == '__main__':

    ### $xpr_param_job_import

  COUNT_TIMES = int(sys.argv[1])
  while COUNT_TIMES:
    print(COUNT_TIMES)
    COUNT_TIMES -= 1
    time.sleep(1)