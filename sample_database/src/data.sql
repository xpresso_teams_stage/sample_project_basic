CREATE DATABASE  IF NOT EXISTS `reporting` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `reporting`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: reporting
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesstokens`
--

DROP TABLE IF EXISTS `accesstokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesstokens` (
  `application` text,
  `token` text,
  `client` text,
  `expiryDate` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesstokens`
--

LOCK TABLES `accesstokens` WRITE;
/*!40000 ALTER TABLE `accesstokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesstokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` VALUES (67,'UBAHomePage','{\"pages\":[{\"security\":{\"allow\":{\"roles\":[\"uba_user\"],\"users\":[]},\"deny\":{\"roles\":[],\"users\":[]}},\"filter_params\":[],\"widgets\":[],\"properties\":{\"name\":\"Applications\",\"url\":\"applications.html\"}},{\"security\":{\"allow\":{\"roles\":[\"uba_user\"],\"users\":[]},\"deny\":{\"roles\":[],\"users\":[]}},\"filter_params\":[],\"widgets\":[],\"properties\":{\"name\":\"Settings\",\"url\":\"settings.html\"}},{\"security\":{\"allow\":{\"roles\":[\"app_manager\"],\"users\":[]},\"deny\":{\"roles\":[],\"users\":[]}},\"filter_params\":[],\"widgets\":[],\"properties\":{\"name\":\"App Manager\",\"url\":\"app_manager.html\"}},{\"security\":{\"allow\":{\"roles\":[\"admin\"],\"users\":[]},\"deny\":{\"roles\":[],\"users\":[]}},\"filter_params\":[],\"widgets\":[],\"properties\":{\"name\":\"Manage Users and Roles\",\"url\":\"admin.html\"}}, {\"security\":{\"allow\":{\"roles\":[\"admin\"],\"users\":[]},\"deny\":{\"roles\":[],\"users\":[]}},\"filter_params\":[],\"widgets\":[],\"properties\":{\"name\":\"Manage Access Tokens\",\"url\":\"access_tokens.html\"}}],\"filter_params\":[],\"id\":1234,\"widgets\":[],\"properties\":{\"owner\":\"krishna.kumar@abzooba.com\",\"image_url\":\"http://abzooba.com/wp-content/uploads/2014/10/logo_wp_site1.png\",\"name\":\"UBAHomePage\",\"theme\":\"dark-blue\",\"title\":\"UBA Home\"}}');
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loginaccesstokens`
--

DROP TABLE IF EXISTS `loginaccesstokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loginaccesstokens` (
  `user_name` varchar(100) NOT NULL,
  `access_token` varchar(256) NOT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loginaccesstokens`
--

LOCK TABLES `loginaccesstokens` WRITE;
/*!40000 ALTER TABLE `loginaccesstokens` DISABLE KEYS */;
INSERT INTO `loginaccesstokens` VALUES ('superuser','RFw2R8WFZ8H3z6BdyUAUbpdIIpl5FkZtsvVWp1OmBVu1wLQFzkM8kmhpvwq8IN1j83wPi2WDhhpeshnFHdkxlGHaOvlWvQTkDIR2LutqLBGnbrpPOK2lUoFc8xsm1DaxrWwjqf6o0oEP1r7QEaGhWWE6zg72hKWJJ5HcomAyssIS3pKo8RgL1Dh2TpWyzWHJkzoGvZJeGKOi7Ydb3s9MH2wchM3tGtpGycpHGf0K1Vrh56zx1UM0LZOOP5JgqY5r','2019-03-17 00:00:00','\0');
/*!40000 ALTER TABLE `loginaccesstokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('admin'),('app_manager'),('uba_user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `role_name` text,
  `user_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES ('admin','superuser'),('app_manager','superuser'),('uba_user','superuser');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_name` text,
  `user_pass` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('superuser','24bd848abd2eca5439014ba014162509');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'reporting'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-16  7:24:41

-- SAGAR DEVIDAS KOLHE